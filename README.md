openingh is a webapp to help finding open shops around.

It also allows to enter missing opening hours and add it to OSM.


## How to deploy with nginx

The following instructions work with Debian. You may need to adapt.

    cd /where/is/
    sudo -u www-data git clone https://bitbucket.org/pitchum/openingh.git

Somewhere in your nginx config:

    location / {
      alias /where/is/openingh/;
    }

