# Changelog


## v0.2

- Human readable day names
- Cleaner UI (no debugging info displayed to the user)
- Using map popups to display the summary of a POI


## v0.1

-  Initial version : consulting opening hours in the vicinity
