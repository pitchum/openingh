// variable that holds current app state.
var app = {
    version: '0.2-alpha',
    cache: {
        last_area_knownoh: null,
        last_area_missingoh: null,
    },
    init: function () {
        console.debug("Initializing app.");
        
        document.querySelector('#app-version').textContent = "v" + app.version;
        
        // Restore saved data
        Object.assign(app.prefs, app.storage.get('prefs', app.prefs));
        var map_center = app.storage.get('map_state.center', [48.825, 2.232]);
        var map_zoom   = app.storage.get('map_state.zoom', 14);
        
        // display the initial map
        this.map = L.map('map').setView(map_center, map_zoom);
        this.map.addLayer(this.tileLayers[this.prefs.tile_layer_name]);
        this.map.on('locationfound', function (evt) {
            if (app.map.getZoom() < 14) {
                app.map.setView(evt.latlng, 14);
            } else {
                app.map.setView(evt.latlng);
            }
            app.map.addLayer(app.layers["known opening hours"]);
        });
        this.map.on('locationerror', function (evt) {
            app.map.addLayer(app.layers["known opening hours"]);
            alert(evt.message);
        });
        this.map.on('popupclose', function(evt) {
            app.close_oh_editor();
        });
        
        // POI layer
        this.layers = {}
        this.layers["known opening hours"] = L.geoJSON(null, {
            style: function (feature) {
                if (feature.properties.tags.opening_hours) {
                    try {
                        var oh = new opening_hours(feature.properties.tags.opening_hours, null, {tag_key: 'opening_hours'});
                        if (oh.getUnknown()) {
                            console.warn("Unknown opening hours", feature);
                            style = {color: "grey"};
                        } else if (oh.getState()) {
                            // console.log(feature.properties.name, "is opened right now");
                            style = {color: "green"};
                        } else {
                            // console.log(feature.properties.name, "is closed right now");
                            style = {color: "red"};
                        }
                    } catch (e) {
                        style = {color: "violet"};
                    }
                } else {
                    console.warn("feature without opening_hours information", feature);
                }
                return style;
            },
            onEachFeature: function(feature, layer) {
                layer.on('click', app.print_info);
            },
            pointToLayer: function (feature, latlng) {
                return new L.circleMarker(latlng);
            },
        });
        
        this.layers["possibly missing"] = L.geoJSON(null, {
            style: function (feature) {
                style = {color: "orange"};
                return style;
            },
            onEachFeature: function(feature, layer) {
                layer.on('click', app.print_info);
            },
            pointToLayer: function (feature, latlng) {
                return new L.circleMarker(latlng);
            },
        });

        
        L.control.layers(app.tileLayers, app.layers).addTo(app.map);
        app.map.on('baselayerchange', function(evt) {
            app.prefs.tile_layer_name = evt.name;
            app.storage.put("prefs", app.prefs);
        });
        app.map.on('overlayadd',    app.on_map_move);
        app.map.on('overlayremove', app.on_map_move);
        
        app.map.on('moveend', app.on_map_move);
//        app.map.on('zoomend', app.on_map_move); XXX disabled because moveend is already fired when zoom changes (probably a bug in leaflet 1.0.3)

        this.change_page("main");
        
        console.debug("Initialized app.");
    },
    
    storage: new Storage(),

    // List of the available tile layers
    tileLayers: {
        "OSM-FR": L.tileLayer('//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
            attribution: 'Rendering:&nbsp;<a href="http://www.openstreetmap.fr/">&copy; OSM-FR</a>',
        }),
        "Wikimedia": L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
            attribution: 'Rendering:&nbsp;<a href="http://www.wikimedia.org/">&copy; Wikimedia</a>',
        }),
        // "Mapnik (international)": L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            // attribution: 'Rendering:&nbsp;Mapnik',
        // }),
        // "Cyclisme": L.tileLayer('http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png', {
            // attribution: 'Rendering:&nbsp;<a href="http://www.thunderforest.com/">&copy; OpenCycleMap</a>',
        // }),
        // "Relief": L.tileLayer('http://{s}.tile3.opencyclemap.org/landscape/{z}/{x}/{y}.png', {
            // attribution: 'Rendering:&nbsp;<a href="http://www.thunderforest.com/">&copy; OpenCycleMap</a>',
        // }),
        "Transports": L.tileLayer('http://{s}.tile2.opencyclemap.org/transport/{z}/{x}/{y}.png', {
            attribution: 'Rendering:&nbsp;<a href="http://www.thunderforest.com/">&copy; OpenCycleMap</a>',
        }),
        "HOT": L.tileLayer('//tile-{s}.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
            attribution: 'Rendering:&nbsp;<a href="https://www.hotosm.org/">&copy; HOT-OSM</a>',
        }),

    },
    
    tasks: {count: 0},
    loading: function() {
        app.tasks.count += 1;
        if (app.tasks.count > 0) {
            document.querySelector('#icon-loading div').style.visibility = 'visible';
        }
    },
    loading_stop: function() {
        app.tasks.count -= 1;
        if (app.tasks.count == 0) {
            document.querySelector('#icon-loading div').style.visibility = 'hidden';
        }
    },
    
    // OSM key/values of POI that may possibly miss opening hours
    poiTags: {
        "general": ["amenity=parking"],
        "public": [
            "amenity=bank",
            "amenity=bureau_de_change",
            "amenity=charging_station", // Charging facility for electric vehicles 
            "amenity=hospital",
            "amenity=library",
            "amenity=pharmacy",
            "amenity=post_office",
        ],
        "entertainment": [
            "amenity=arts_centre",
            "amenity=brothel",
            "amenity=cinema",
            "amenity=casino",
            "amenity=community_centre",
            "amenity=nightclub",
            "amenity=theatre",
        ],
        "shop": [
            "shop",
            "amenity=bar",
            "amenity=cafe",
            "amenity=fast_food",
            "amenity=fuel",
            "amenity=pub",
            "amenity=restaurant",
        ],
        "education": [
            "amenity=college",
            "amenity=kindergarten",
            "amenity=public_bookcase",
            "amenity=school",
            "amenity=music_school",
            "amenity=driving_school",
            "amenity=language_school",
            "amenity=university",
        ],
        "healthcare": [
            "amenity=clinic",
            "amenity=dentist",
            "amenity=doctors",
        ],
    },
    
    // user preferences, persisted to/from localStorage
    prefs: {
        tile_layer_name: 'Wikimedia',
        overpass_instance: '//overpass-api.de/api/interpreter/',
        // '//api.openstreetmap.fr/oapi/interpreter/'
        // '//overpass-api.de/api/interpreter/'
        // 'http://overpass.osm.rambler.ru/cgi/interpreter/' // XXX no HTTPS support
    },
    
    
    on_map_move: function (evt) {

        if (app.map.hasLayer(app.layers["known opening hours"])) {
            if (!app.cache.last_area_knownoh || !app.cache.last_area_knownoh.contains(app.map.getCenter())) {
                app.loading();
                overpass_query = app.build_query_pois({"opening_hours": ""});
                app.fetch_pois(overpass_query, function(geojson) {
                    app.layers["known opening hours"].clearLayers();
                    app.layers["known opening hours"].addData(geojson);
                    app.loading_stop();
                });
            }
        }
        
        if (app.map.hasLayer(app.layers["possibly missing"])) {
            if (!app.cache.last_area_missingoh || !app.cache.last_area_missingoh.contains(app.map.getCenter())) {
                app.loading();
                var tags = [];
                Object.keys(app.poiTags).forEach(function(k, i) {
                    var pairs = app.poiTags[k];
                    for (var i = 0; i < pairs.length; i++) {
                        tags.push(pairs[i]);
                    }
                });
                overpass_query = app.build_query_missing(tags);
                app.fetch_pois(overpass_query, function(geojson) {
                    app.layers["possibly missing"].clearLayers();
                    app.layers["possibly missing"].addData(geojson);
                    app.loading_stop();
                });
            }
        }
        
        // remember map state
        app.storage.put('map_state.center', app.map.getCenter());
        app.storage.put('map_state.zoom', app.map.getZoom());
    },
    
    // Build an overpass query using a JSON structure as key/value pair filters.
    build_query_pois: function (kv) {
        var q = "";
        for (var key in kv) {
            var val = kv[key];
            if (val) {
                q += '    node["' + key + '"="' + val + '"]({{bbox}});\n';
                q += '     way["' + key + '"="' + val + '"]({{bbox}});\n';
                q += 'relation["' + key + '"="' + val + '"]({{bbox}});\n';
            } else {
                q += '    node["' + key + '"]({{bbox}});\n';
                q += '     way["' + key + '"]({{bbox}});\n';
                q += 'relation["' + key + '"]({{bbox}});\n';
            }
        }
        
        // Restrict search zone to 900m around the current center of the map.
        var circ = new L.circle(app.map.getCenter(), 500, {opacity: 0}).addTo(this.map);
        var bbox = circ.getBounds();
        app.cache.last_area_knownoh = bbox.pad(0.5);
        // new L.Rectangle(app.cache.last_area_knownoh, {color: 'red', opacity: 0.1}).addTo(app.map); // XXX DEBUG
        // new L.Rectangle(bbox, {color: "grey", opacity: 0.5}).addTo(app.map); // XXX DEBUG
        
        this.map.removeLayer(circ);
        // Cf. http://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL#Global_bounding_box_.28bbox.29
        q = q.replace(/{{bbox}}/g, bbox.getSouth() + "," + bbox.getWest() + "," + bbox.getNorth() + "," + bbox.getEast());
        
        return "[out:json][timeout:25];"
            + "("
            + q
            + ");"
            + "out body;"
            + ">;"
            + "out skel qt;";
    },
    
    // Build an overpass query excluding items having an 'opening_hours' key
    build_query_missing: function (pairs) {
        var q = "";
        pairs.forEach(function(p) {
            if (p.includes("=")) {
                p = p.replace("=", '"="');
            }
            q += '    node["' + p + '"]["opening_hours"!~".*"]({{bbox}});\n';
            q += '     way["' + p + '"]["opening_hours"!~".*"]({{bbox}});\n';
            q += 'relation["' + p + '"]["opening_hours"!~".*"]({{bbox}});\n';
        });
        
        // Restrict search zone to 900m around the current center of the map.
        var circ = new L.circle(app.map.getCenter(), 200, {opacity: 0}).addTo(this.map);
        var bbox = circ.getBounds();
        app.cache.last_area_missingoh = bbox.pad(0.8);
        // new L.Rectangle(app.cache.last_area_missingoh, {color: 'red', opacity: 0.1}).addTo(app.map); // XXX DEBUG
        // new L.Rectangle(bbox, {color: "grey", opacity: 0.5}).addTo(app.map); // XXX DEBUG
        this.map.removeLayer(circ);
        // Cf. http://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL#Global_bounding_box_.28bbox.29
        q = q.replace(/{{bbox}}/g, bbox.getSouth() + "," + bbox.getWest() + "," + bbox.getNorth() + "," + bbox.getEast());
        
        return "[out:json][timeout:25];"
            + "("
            + q
            + ");"
            + "out body;"
            + ">;"
            + "out skel qt;";
    },
    
    
    // Execute an overpass query with ajax.
    fetch_pois: function (overpass_query, callback) {
        fetch(app.prefs.overpass_instance, {
                method: "POST",
                headers: new Headers({"X-Requested-With": "XMLHttpRequest",}),
                body: overpass_query,
            }
        ).then(function(response) {
            if (response.status == 200) {
                return response.json();
            } else {
                console.error(response);
                return {};
            }
        }).then(function (blob) {
            var geojson = osmtogeojson(blob);
            callback(geojson);
        }).catch(function(error) {
            if (error.status == 429) {
                // Too Many Requests
                alert(error.statusText);
            }
            console.error('There has been a problem with your fetch operation: ' + error.message);
        });
    },
    
    // Create a summary text for a given POI.
    gen_info_label: function(props) {
        var oh_raw = props.opening_hours === undefined ? "" : props.opening_hours;
        document.querySelector('#oh-raw-value').value = "";
        document.querySelector('#oh-raw-value').value = oh_raw;

        var title = "";
        // Try to compute a convenient title with the available properties
        if (props.name) {
            title = props.name;
        } else if (props.amenity) {
            title = props.amenity;
        } else if (props.shop) {
            title = props.shop;
        } else {
            console.warn("no title found with", JSON.stringify(Object.keys(props), null, ' '));
        }
        var r = '<div class="poi-info"><em><strong>' + title + '</strong></em>';
        try {
            var oh = new opening_hours(oh_raw, null, {tag_key: 'opening_hours'});
            var state      = oh.getState(); // we use current date
            var unknown    = oh.getUnknown();
            var comment    = oh.getComment();
            var nextchange = oh.getNextChange();
            if (oh.getUnknown()) {
                r += "<br/>No opening hours found\n";
            } else if (oh.getState()) {
                r += "<br/>Currently <em>open</em>.\n";
            } else {
                r += "<br/>Currently <em>closed</em>.\n";
            }
            
            if (typeof nextchange === 'undefined') {
                r += 'And will never ' + (state ? 'close' : 'open');
            } else {
                r += 'And we will '
                    + (oh.getUnknown(nextchange) ? 'maybe ' : '')
                    + (state ? 'close' : 'open') + " in ";
                var now = new Date();
                var future = new Date(nextchange);
                var delta = (future - now) / 60000; // delta in minutes
                if (delta >  1440) {
                    delta = Math.round(delta / 14440);
                    r += delta + " days";
                } else if (delta > 60) {
                    delta = Math.round(delta / 60);
                    r += delta + " hours";
                } else {
                    r += Math.round(delta) + " minutes";
                }
            }
        } catch (e) {
            r += "<br/>" + oh_raw;
        }
        r+= '</div>';
        return r;
    },
    
    // print information for a POI (a geoJSON "feature")
    print_info: function (evt) {
        var item = evt.target;
        var anchor;
        if ("getLatLng" in item) {
            // if selected item is a point, put the new marker on it
            anchor = item.getLatLng();
        } else if ("getCenter" in item) {
            // if selected item is a polygon, put the new marker at the center of it
            anchor = item.getCenter();
        }
        app.cache.popup = L.popup({maxWidth: 150, autoPanPadding: [50,50]})
            .setLatLng(anchor)
            .setContent(app.gen_info_label(item.feature.properties.tags));
        // Ensure the zoom level is greater than 16
        if (app.map.getZoom() < 16) {
            app.map.setView(anchor, 16);
            setTimeout(function(){
                app.map.openPopup(app.cache.popup);
            }, 300);
        } else {
            app.map.openPopup(app.cache.popup);
        }
        
        // document.querySelector('#detailed-info').innerText = JSON.stringify(item.feature.properties.tags, null, ' ');
        app.open_oh_editor();
        app.update_oh_renderer();
    },
    
    // Page navigation
    nav: { current_page: "main" },
    change_page: function (pagename) {
        var tgtpage = document.querySelector('section[role="region"]#' + pagename + "-page");
        if (!tgtpage) {
            return false;
        }
        var curpage = document.querySelector('section[role="region"]#' + app.nav.current_page + "-page");
        app.nav.current_page = pagename;
        curpage.style.display = 'none';
        tgtpage.style.display = 'block';
    },


    // Opening hours editor
    open_oh_editor: function() {
        if ('closing_editor_lock' in app.cache) {
            clearTimeout(app.cache.closing_editor_lock);
        }
        var oheditor = document.querySelector('#oh-editor');
        oheditor.style.display = 'block';
        document.querySelector('#map').style.height = '12rem';
        app.map.invalidateSize();
    },
    
    close_oh_editor: function() {
        var real_f = function() {
            var oheditor = document.querySelector('#oh-editor');
            oheditor.style.display = 'none';
            document.querySelector('#map').style.height = '20rem';
            app.map.invalidateSize();
        };
        app.cache.closing_editor_lock = setTimeout(real_f, 100);
    },
    
    update_oh_renderer: function() {
        var ohval = document.querySelector('#oh-raw-value').value;
        var gendiv = function(a, b, clazz) {
            var duration = b - a;
            var percent = Math.floor(duration * 10000.0 / (1000*60*60*24)) / 100;
            return '<div class="'+clazz+'" style="width: '+percent+'%;"></div>';
        };
        try {
            var oh = new opening_hours(ohval, null, {tag_key: 'opening_hours'});
            for (var day = 0; day < 7; day++) {
                var start = new Date();
                start.setDate(start.getDate() + day);
                start.setHours(0);
                start.setMinutes(0);
                start.setSeconds(0);
                var end = new Date(start);
                end.setDate(end.getDate() + 1);
                cur = "";
                var intervals = oh.getOpenIntervals(start, end);
                if (intervals.length) {
                    for (var i = 0; i < intervals.length; i++) {
                        var a = intervals[i][0];
                        var b = intervals[i][1];
                        cur += gendiv(start, a, 'closed');
                        cur += gendiv(a, b, 'open');
                        start = b;
                    }
                } else {
                    cur += '<div class="closed"></div>';
                }
                
                // Compute day name
                var daylabel = start.toLocaleDateString(undefined, { weekday: 'short'})
                var th = document.querySelector("#day" + (1 + day) + " th");
                th.innerHTML = daylabel;
                
                var td = document.querySelector("#day" + (1 + day) + " td");
                td.innerHTML = cur;
            }
            document.querySelector('#oh-editor').style.display = 'block';
        } catch(e) {
            console.error("Failed to render OH expression:", ohval, e);
            document.querySelector('#oh-editor').style.display = 'none';
        }
    },
    


};

function Storage() {
  this.backend = localStorage;

  this.put = function(key, val) {
      this.backend.setItem(key, JSON.stringify(val));
  };
  this.get = function(key, default_val) {
      var tmp = this.backend.getItem(key);
      if (tmp == null) {
        return default_val;
      }
      try {
        return JSON.parse(tmp);
      } catch (e) {
        return tmp;
      }
    }

}


document.querySelector('#oh-raw-value').addEventListener('change', function(evt) {
    app.update_oh_renderer();
});


app.init();
app.map.locate();
